# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue with the maintainers of this repository before making a change.

## Merge Requests

1. Ensure you pass the existing tests and create any extra tests if necessary.
2. Document your changes in CHANGELOG.md
