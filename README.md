# uwsgi-nginx

Base docker image for deploying Python web application, like Flask and Django.

## Usage

```dockerfile
FROM joshava/uwsgi-nginx
```

You should put your application in `/app` with `run.py` and the variable named `app`.
You can use your own `uwsgi.ini` if needed.

### Ports

`80`: Web port.
